<?php
include './includes/config.php';
if(isset($_GET['user_id'])) {
    $user_id = $_GET['user_id'];
    $profile = "N";
} else {
    $user_id = $_SESSION['user_id'];
    $profile = "Y";
}
$q = "SELECT * from tbl_user where user_id = '$user_id' ";
$r = $con->query($q) or die ($q);
$obj = $r->fetch_object();
?>
<div class="row justify-content-center">
<br>
<div class="col-md-6">
    <form id="form_reg">
        <input type="hidden" name="user_id" value="<?php echo $obj->user_id;?>" >
        <input type="hidden" name="profile" value="<?php echo $profile;?>" >
        <input type="hidden" name="user_type_id" value="<?php echo $obj->user_type_id;?>" >
    <div class="card">
        <div class="card-header">
            แก้ไขข้อมูลส่วนตัว
        </div>
        <div class="card-body">

                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">ชื่อเข้าใข้:</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="usr" name="usr" placeholder="username" value="<?php echo $obj->username;?>" readonly required>
                        </div>
                </div>
                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">กำหนดรหัสผ่าน:</label>
                        <div class="col-sm-9">
                        <input type="password" class="form-control" id="pwd" name="pwd" placeholder="password" value="<?php echo $obj->user_password;?>" required>
</div>
                </div>
                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">ยืนยันรหัสผ่าน:</label>
                        <div class="col-sm-9">
                        <input type="password" class="form-control" id="pwd2" name="pwd2" placeholder="password" required>
</div>
                </div>

                <div class="form-group row">
                   
                        <label class="col-sm-3 col-form-label">คำนำหน้า:</label>
                        <div class="col-sm-9">

                        <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="นาย" <?php if($obj->tname=='นาย') echo "checked";?> name="tname">นาย
                        </label>
                        </div>
                        <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="นาง" name="tname" <?php if($obj->tname=='นาง') echo "checked";?> >นาง
                        </label>
                        </div>
                        <div class="form-check-inline disabled">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="นางสาว" name="tname"  <?php if($obj->tname=='นางสาว') echo "checked";?> >นางสาว
                        </label>
                        </div>

                        </div>
                </div>

                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">ชื่อจริง:</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="fname" name="fname" placeholder="ชื่อ"  value="<?php echo $obj->fname;?>" required>
                        </div>
                </div>

                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">นามสกุล:</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="lname" name="lname" placeholder="นามสกุล" value="<?php echo $obj->lname;?>" required>
                        </div>
                </div>

                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">รหัสประจำตัว:</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="user_code" name="user_code" placeholder="606xxxxxxx" value="<?php echo $obj->user_code;?>" required>
                        </div>
                </div>

                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">เบอร์โทร:</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="user_tel" name="user_tel" placeholder="09xxxxxxx" value="<?php echo $obj->user_tel;?>" required>
                        </div>
                </div>

                <div class="form-group row">
                        <label class="col-sm-3 col-form-label">อีเมล์:</label>
                        <div class="col-sm-9">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="123@aaa.bb" value="<?php echo $obj->user_email;?>" required>
                        </div>
                </div>

        </div>
        <div class="card-footer">
            <button class="btn btn-primary" type="submit" > <i class="fa fa-save"></i> บันทึกข้อมูล</button>
            <span id="show_save" class="text-danger"></span>
        </div>
    </div> <!-- card -->
    </form>
    
</div>

</div> <!-- row -->
<script>
$('#form_reg').submit(function(e){
    e.preventDefault();
    var data = $('#form_reg').serialize();
    $.post("register_save.php",data,function(info){
        var obj = jQuery.parseJSON(info);
        if(obj.save == 'ok'){
            alert("แก้ไขข้อมูลสำเร็จ");
            if(obj.profile == 'N' && obj.user_type_id=='1') {
                window.location = '../../educationstd/manage/?page=main';
            } else if(obj.profile == 'N' && obj.user_type_id=='2'){
                window.location = '../../educationstd/manage/?page=student_list';
            } else if(obj.profile == 'N' && obj.user_type_id=='3') {
                window.location = '../../educationstd/manage/?page=person_list';
            } else {
                window.location = '?page=main';
            }
            
        } else {
            $('#show_save').html(obj.save);
        }
    });
});
</script>