<?php
session_start();
include './includes/function.php';
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "main";
}

if (isset($_SESSION['user_id'])) {
    $logined = true;
    $user_type_id = $_SESSION['user_type_id'];

} else {

    if($page != 'register') {
        $page = 'login';
    }
    
    $logined = false;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบริหารจัดการรายวิชาโครงงาน</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/style.css">
    <link rel="stylesheet" href="./node_modules/select2/dist/css/select2.min.css">
    
    <link href="./css/all.css" rel="stylesheet">

    

    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/popper.js/dist/umd/popper.min.js"></script>
    <!-- <script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script> -->
    <script src="./node_modules/select2/dist/js/select2.min.js"></script>

    <script src="./node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <link href="./node_modules/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
   
    
</head>
<body>
<?php include './includes/header.php'; ?>

<?php 
    include $page.".php"; 
?>

<?php include './includes/footer.php'; ?>
</body>
</html>