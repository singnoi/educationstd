/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : educationstd_db

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-03-29 14:11:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_do
-- ----------------------------
DROP TABLE IF EXISTS `tbl_do`;
CREATE TABLE `tbl_do` (
  `do_id` int(11) NOT NULL AUTO_INCREMENT,
  `do_name` varchar(200) DEFAULT NULL,
  `do_st_date` date DEFAULT NULL,
  `do_en_date` date DEFAULT NULL,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_do
-- ----------------------------
INSERT INTO `tbl_do` VALUES ('1', 'เขียนโครงการเพื่อเสนอขออนุมัติ', '2019-03-15', '2019-04-13');
INSERT INTO `tbl_do` VALUES ('2', 'ติดต่อวิทยากร', '2019-03-16', '2019-03-18');

-- ----------------------------
-- Table structure for tbl_indicator
-- ----------------------------
DROP TABLE IF EXISTS `tbl_indicator`;
CREATE TABLE `tbl_indicator` (
  `indicator_id` int(11) NOT NULL AUTO_INCREMENT,
  `indicator_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`indicator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_indicator
-- ----------------------------
INSERT INTO `tbl_indicator` VALUES ('1', 'ร้อยละของความสำเร็จและกระบวนการ');
INSERT INTO `tbl_indicator` VALUES ('2', 'ระดับความพึงพอใจ');

-- ----------------------------
-- Table structure for tbl_methods
-- ----------------------------
DROP TABLE IF EXISTS `tbl_methods`;
CREATE TABLE `tbl_methods` (
  `methods_id` int(11) NOT NULL AUTO_INCREMENT,
  `methods_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `methods_eng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `methods_type` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `strategy_id` int(11) DEFAULT NULL,
  `tactics_id` int(11) DEFAULT NULL,
  `indicator_id` int(11) DEFAULT NULL,
  `std_sko_id` int(11) DEFAULT NULL,
  `owner_user_id` int(11) DEFAULT NULL COMMENT 'อาจารย์ผู้รับผิดชอบ',
  `slave_user_id` int(11) DEFAULT NULL COMMENT 'ผู้ช่วยอาจารย์',
  `owner_etc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'วิทยากร',
  `instructor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `becouseoff` text COLLATE utf8_unicode_ci COMMENT 'หลักการและเหตุผล',
  `objective` text COLLATE utf8_unicode_ci COMMENT 'วัตถุประสงค์',
  `target_group` text COLLATE utf8_unicode_ci COMMENT 'กลุ่มเป้าหมาย',
  `do_id` int(11) DEFAULT NULL COMMENT 'ระยะดำเนินการ',
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'สถานที่',
  `budget_id` int(11) DEFAULT NULL COMMENT 'งบประมาณ',
  `results` text COLLATE utf8_unicode_ci COMMENT 'ผลที่คาดว่า',
  `schedule_id` int(11) DEFAULT NULL COMMENT 'กำหนดการ',
  `syear` int(4) DEFAULT NULL,
  PRIMARY KEY (`methods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_methods
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_methods_std
-- ----------------------------
DROP TABLE IF EXISTS `tbl_methods_std`;
CREATE TABLE `tbl_methods_std` (
  `methods_id` int(11) DEFAULT NULL,
  `std_sko_id` int(11) DEFAULT NULL,
  `std_sub_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_methods_std
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_plan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_plan`;
CREATE TABLE `tbl_plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_no` int(11) NOT NULL,
  `plan_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tactics_id` int(11) NOT NULL,
  `strategy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_plan
-- ----------------------------
INSERT INTO `tbl_plan` VALUES ('1', '1', 'การใช้พื้นที่ของมหาวิทยาลัย', '1', '1');
INSERT INTO `tbl_plan` VALUES ('2', '1', 'การจัดการใช้พลังงานภายในมหาวิทยาลัย', '3', '1');
INSERT INTO `tbl_plan` VALUES ('3', '2', 'การลดภาวะเรือนกระจกและการปล่อยก๊าซ', '3', '1');
INSERT INTO `tbl_plan` VALUES ('4', '1', 'การจัดการของเสียภายในมหาวิทยาลัย', '4', '1');
INSERT INTO `tbl_plan` VALUES ('5', '2', 'การบำบัดของเสียและการใช้ประโยชน์จากการบำบัด', '4', '1');
INSERT INTO `tbl_plan` VALUES ('6', '1', 'การบริหารจัดการน้ำภายในมหาวิทยาลัย', '5', '1');
INSERT INTO `tbl_plan` VALUES ('7', '1', 'การปรับปรุงและพัฒนา ระบบการบริหารและการจัดการจำนวนและที่จอดรถภายในมหาวิทยาลัย', '6', '1');
INSERT INTO `tbl_plan` VALUES ('8', '2', 'ลดการใช้พาหนะที่ปล่อยก๊าซ CO2 และสนบสนุนการเดินตามทางเท้าภายในมหาวิทยาลัย', '6', '1');
INSERT INTO `tbl_plan` VALUES ('9', '1', 'พัฒนาหลักสูตรเกี่ยวกับสิ่งแวดล้อม และความยั่งยืน', '7', '1');
INSERT INTO `tbl_plan` VALUES ('10', '1', 'พัฒนาตามกรอบมาตรฐานคุณวุฒิอุดมศึกษา (TQF)', '8', '3');
INSERT INTO `tbl_plan` VALUES ('11', '1', 'พัฒนาทักษะการเรียนรู้และนวัตกรรม', '9', '3');
INSERT INTO `tbl_plan` VALUES ('12', '2', 'พัฒนาทักษะสารสนเทศ สื่อ และเทคโนโลยี', '9', '3');
INSERT INTO `tbl_plan` VALUES ('13', '3', 'พัฒนาทักษะชีวิตและอาชีพ', '9', '3');
INSERT INTO `tbl_plan` VALUES ('14', '1', 'พัฒนาด้าน 3R อ่านออก เขียนได้ คิดเป็น', '10', '3');
INSERT INTO `tbl_plan` VALUES ('15', '2', 'พัฒนาด้าน 8C', '10', '3');
INSERT INTO `tbl_plan` VALUES ('16', '1', 'การสนับสนุนทุนการศึกษาของนักศึกษา ด้านต่างๆ', '11', '3');
INSERT INTO `tbl_plan` VALUES ('17', '1', 'การสร้างองค์ความรู้เพื่อพัฒนาด้านการเรียนการสอน', '12', '4');
INSERT INTO `tbl_plan` VALUES ('18', '1', 'การส่งเสริมด้านการวิจัย', '13', '4');
INSERT INTO `tbl_plan` VALUES ('19', '2', 'การสร้างองค์ความรู้เพื่อพัฒนาการให้บริการทางวิชาการ', '13', '4');
INSERT INTO `tbl_plan` VALUES ('20', '1', 'ส่งเสริมการให้บริการวิชาการแก่สังคม', '14', '5');
INSERT INTO `tbl_plan` VALUES ('21', '1', 'พัฒนาและจัดตั้งแหล่งเรียนรู้เพื่อบริการวิชาการแก่สังคม', '15', '5');
INSERT INTO `tbl_plan` VALUES ('22', '1', 'สร้างเครือข่ายความร่วมมือกับองค์กรภาครัฐและเอกชน', '16', '5');
INSERT INTO `tbl_plan` VALUES ('23', '2', 'สร้างเครือข่ายความร่วมมือด้านการประชาสัมพันธ์', '16', '5');
INSERT INTO `tbl_plan` VALUES ('24', '1', 'ส่งเสริมและอนุรักษ์ศิลปวัฒนธรรมและภูมิปัญญาท้องถิ่น', '17', '6');
INSERT INTO `tbl_plan` VALUES ('25', '2', 'พัฒนาและเผยแพร่ศิลปวัฒนธรรมและภูมิปัญญาท้องถิ่น', '17', '6');
INSERT INTO `tbl_plan` VALUES ('26', '1', 'พัฒากระบวนการปฏิบัติงานเพื่อเพิ่มผลิตภาพให้คุ้มค่า', '18', '7');
INSERT INTO `tbl_plan` VALUES ('27', '2', 'พัฒากระบวนการการมีส่วนร่วมของบุคลากร', '18', '7');
INSERT INTO `tbl_plan` VALUES ('28', '3', 'พัฒาระบบการกระจายอำนาจและความรับผิดชอบ', '18', '7');
INSERT INTO `tbl_plan` VALUES ('29', '4', 'ปรับปรุงกฏระเบียบให้เกิดความเป็นธรรมและโปร่งใส', '18', '7');
INSERT INTO `tbl_plan` VALUES ('30', '1', 'ส่งเสริมสมรรถนะบุคลากรด้านวิชาการ', '19', '8');
INSERT INTO `tbl_plan` VALUES ('31', '2', 'ส่งเสริมสมรรถนะบุคลากรด้านวิชาชีพ', '19', '8');
INSERT INTO `tbl_plan` VALUES ('32', '1', 'ส่งเสริมการศึกษาต่อของบุคลากร', '20', '8');
INSERT INTO `tbl_plan` VALUES ('33', '2', 'ส่งเสริมการผลิตผลงานทางวิชาการของบุคลากร', '20', '8');
INSERT INTO `tbl_plan` VALUES ('34', '3', 'สนับสนุนการเข้าร่วมประชุม/อบรม/สัมมนา/ศึกษาดูงานของบุคลากร', '20', '8');

-- ----------------------------
-- Table structure for tbl_schedule
-- ----------------------------
DROP TABLE IF EXISTS `tbl_schedule`;
CREATE TABLE `tbl_schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `methods_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_schedule_time
-- ----------------------------
DROP TABLE IF EXISTS `tbl_schedule_time`;
CREATE TABLE `tbl_schedule_time` (
  `schedule_time_id` int(11) NOT NULL AUTO_INCREMENT,
  `begin_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `topic` text COLLATE utf8_unicode_ci,
  `schedule_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`schedule_time_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_schedule_time
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_standard_sko
-- ----------------------------
DROP TABLE IF EXISTS `tbl_standard_sko`;
CREATE TABLE `tbl_standard_sko` (
  `std_sko_id` int(11) NOT NULL AUTO_INCREMENT,
  `std_sko_no` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `std_sko_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `std_sko_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `std_sko_level` enum('1','2','3') COLLATE utf8_unicode_ci DEFAULT '1',
  PRIMARY KEY (`std_sko_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_standard_sko
-- ----------------------------
INSERT INTO `tbl_standard_sko` VALUES ('1', '3.2', 'การส่งเสริมและพัฒนานักศึกษา', 'กระบวนการ', '1');
INSERT INTO `tbl_standard_sko` VALUES ('2', '1.6', 'กิจกรรมนักศึกษาระดับปริญญาตรี', 'กระบวนการ', '2');
INSERT INTO `tbl_standard_sko` VALUES ('3', '1.5', 'กิจกรรมนักศึกษาระดับปริญญาตรี', 'กระบวนการ', '3');

-- ----------------------------
-- Table structure for tbl_strategy
-- ----------------------------
DROP TABLE IF EXISTS `tbl_strategy`;
CREATE TABLE `tbl_strategy` (
  `strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `strategy_no` int(11) NOT NULL,
  `strategy_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`strategy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_strategy
-- ----------------------------
INSERT INTO `tbl_strategy` VALUES ('1', '1', 'พัฒนามหาวิทยาลัยให้เป็น Green University');
INSERT INTO `tbl_strategy` VALUES ('3', '2', 'พัฒนาบัณฑิตให้ได้บัณฑิตที่มีคุณลักษณะที่พึงประสงค์และตรงตามความต้องการของสังคมโดยการพัฒนาอย่างต่อเนื่อง');
INSERT INTO `tbl_strategy` VALUES ('4', '3', 'พัฒนาและสร้างองค์ความรู้ใหม่เพื่อบริการแก่สังคม');
INSERT INTO `tbl_strategy` VALUES ('5', '4', 'พัฒนาแหล่งความรู้และบริการแก่สังคม โดยสร้างความร่วมมือกับองค์กรภาครัฐและเอกชน');
INSERT INTO `tbl_strategy` VALUES ('6', '5', 'การอนุรักษ์ ส่งเสริมศิลปวัฒนธรรมและภูมิปัญญาท้องถิ่น');
INSERT INTO `tbl_strategy` VALUES ('7', '6', 'การบริหารมหาวิทยาลัยโดยใช้หลักธรรมาภิบาลเพื่อสร้างความแข็งแกร่งและยั่งยืน');
INSERT INTO `tbl_strategy` VALUES ('8', '7', 'พัฒนาบุคลากรให้มีสมรรถนะที่สูงขึ้น');

-- ----------------------------
-- Table structure for tbl_tactics
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tactics`;
CREATE TABLE `tbl_tactics` (
  `tactics_id` int(11) NOT NULL AUTO_INCREMENT,
  `tactics_no` int(11) NOT NULL,
  `tactics_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `strategy_id` int(11) NOT NULL,
  PRIMARY KEY (`tactics_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_tactics
-- ----------------------------
INSERT INTO `tbl_tactics` VALUES ('1', '1', 'การวางระบบโครงสร้างพื้นฐาน (Setting and infrastructure)', '1');
INSERT INTO `tbl_tactics` VALUES ('3', '2', 'การจัดการพลังงานและการเปลี่ยนแปลงภูมิอากาศ (Energy and Climate Change)', '1');
INSERT INTO `tbl_tactics` VALUES ('4', '3', 'การจัดการของเสีย (Waste Management)', '1');
INSERT INTO `tbl_tactics` VALUES ('5', '4', 'การจัดการน้ำ (Water management)', '1');
INSERT INTO `tbl_tactics` VALUES ('6', '5', 'การคมนาคมขนส่ง (Transportation) ที่เป็นมิตรกับสิ่งแวดล้อม', '1');
INSERT INTO `tbl_tactics` VALUES ('7', '6', 'แผนการศึกษา (Education) ที่ตระหนักถึงการอนุรักษ์สิ่งแวดล้อม และความยั่งยืน', '1');
INSERT INTO `tbl_tactics` VALUES ('8', '1', 'การพัฒนาตามกรอบมาตรฐานคุณวุฒิอุดมศึกษา (TQF)', '3');
INSERT INTO `tbl_tactics` VALUES ('9', '2', 'การพัฒนาทักษะการเรียนรู้ ศตวรรษที่ 21', '3');
INSERT INTO `tbl_tactics` VALUES ('10', '3', 'การพัฒนาทักษะของคนใน ศตวรรษที่ 21 ด้าน 3R+8C', '3');
INSERT INTO `tbl_tactics` VALUES ('11', '4', 'การสนับสนุนทุนการศึกษาแก่นักศึกษา', '3');
INSERT INTO `tbl_tactics` VALUES ('12', '1', 'พัฒนาและสร้างองค์ความรู้เพื่อการพัฒนาด้านการเรียนการสอน', '4');
INSERT INTO `tbl_tactics` VALUES ('13', '2', 'พัฒนาและสร้างองค์ความรู้เพื่อการพัฒนาด้านการวิจัยและบริการวิชาการ', '4');
INSERT INTO `tbl_tactics` VALUES ('14', '1', 'พัฒนากระบวนการบริการวิชาการแก่สังคม', '5');
INSERT INTO `tbl_tactics` VALUES ('15', '2', 'พัฒนาและจัดตั้งแหล่งเรียนรู้ทางวิชาการ', '5');
INSERT INTO `tbl_tactics` VALUES ('16', '3', 'สร้างเครือข่ายความร่วมมือกับองค์กรภาครัฐและเอกสน', '5');
INSERT INTO `tbl_tactics` VALUES ('17', '1', 'ส่งเสริม พัฒนา และอนุรักษ์ ศิลปวัฒนธรรมและภูมิปัญญาท้องถิ่น', '6');
INSERT INTO `tbl_tactics` VALUES ('18', '1', 'พัฒนากระบวนการปฏิบัติงานอย่างต่อเนื่อง', '7');
INSERT INTO `tbl_tactics` VALUES ('19', '1', 'สร้างสมรรถนะบุคลากรให้เป็นเลิศด้านวิชาการและวิชาชีพ', '8');
INSERT INTO `tbl_tactics` VALUES ('20', '2', 'พัฒนาศักยภาพของบุคลกรสายวิชาการและสายสนับสนุน', '8');
INSERT INTO `tbl_tactics` VALUES ('22', '2', 'การอบรม', '8');

-- ----------------------------
-- Table structure for tbl_unit
-- ----------------------------
DROP TABLE IF EXISTS `tbl_unit`;
CREATE TABLE `tbl_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_unit
-- ----------------------------
INSERT INTO `tbl_unit` VALUES ('1', 'ร้อยละ');

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tname` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `admin` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('1', '0004', '0004', 'นาย', 'พลวัชร์', 'จันทรมงคล', 'poll@gmail.com', '064860588', '6062210001', '1', 'N');
INSERT INTO `tbl_user` VALUES ('4', '0003', '0003', 'นาย', 'ทองปาน', 'ปริวัตร', 'thongphan@gmail.com', '0918617832', '600212', '1', 'Y');
INSERT INTO `tbl_user` VALUES ('6', 'admin', '1234', 'นางสาว', 'พิมลศิริ', 'สีทอง', 'pimolsiri.srrthong@gmail.com', '0987654304', '6062210001', '2', 'Y');
INSERT INTO `tbl_user` VALUES ('7', '6062210002', '6062210002', 'นางสาว', 'ปาลิตา', 'นิ่มน้อย', 'palita@gmail.com', '0893476308', '6062210002', '2', 'N');
INSERT INTO `tbl_user` VALUES ('8', '6062210003', '6062210003', 'นาย', 'ลีปาว', 'ซ่ง', 'le@gmail.com', '0598454955', '6062210003', '2', 'N');
INSERT INTO `tbl_user` VALUES ('9', '6062210005', '6062210005', 'นางสาว', 'เมธาวดี', 'จันทร์หมื่นไว', 'metha@gmail.com', '0904538549', '6062210004', '2', 'N');
INSERT INTO `tbl_user` VALUES ('10', '6062210004', '6062210004', 'นางสาว', 'ศิริจรรยา', 'ประโกทะสังข์', 'siri@gmail.com', '0859846483', '606221005', '2', 'N');
INSERT INTO `tbl_user` VALUES ('11', '6062280001', '6062280001', 'นาย', 'รัชเดช', 'สิงห์น้อย', 'rachadej@gmail.com', '0974529150', '6062280001', '2', 'N');
INSERT INTO `tbl_user` VALUES ('12', 'Athena', '1234', 'นาย', 'กฤติพล', ' ศรีบัวลา', 'Athena@gmail.com', '0984544533', '6062210011', '3', 'N');
INSERT INTO `tbl_user` VALUES ('13', 'Amelia', '1234', 'นางสาว', 'ณัฐรดา ', 'ถนอมทุน', 'Amelia@gmail.com', '0853632522', '6062210012', '3', 'N');
INSERT INTO `tbl_user` VALUES ('14', '0001', '0001', 'นาย', 'พิชัย', 'ระเวงวัน', 'pipi@gmail.com', '0974529150', 'ุ600001', '1', 'N');
INSERT INTO `tbl_user` VALUES ('15', '0002', '0002', 'นาย', 'พันธวุธ', 'จันทรมงคล', 'TTT@gmail.com', '0598454978', '600003', '1', 'N');

-- ----------------------------
-- Table structure for tbl_user_type
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_type`;
CREATE TABLE `tbl_user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user_type
-- ----------------------------
INSERT INTO `tbl_user_type` VALUES ('1', 'อาจารย์');
INSERT INTO `tbl_user_type` VALUES ('2', 'นักศึกษา');
INSERT INTO `tbl_user_type` VALUES ('3', 'บุคคลภายนอก');
