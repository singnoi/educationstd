<?php
if($logined){
    if($_SESSION['admin']=="Y") {
        ?>
        <script>window.location = '../../educationstd/manage/';</script>
    <?php
    } else {
        ?>
        <script>window.location = '../../educationstd/';</script>
            <?php
    }
    
    exit();
}
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-lock"></i> เข้าสู่ระบบ </li>

    </ol>

</nav>
<div class="row justify-content-center">

        <div class="col-md-4">
            <form id="form_login">
            <div class="card alert-warning">
                <div class="card-header bg-warning">
                    เข้าระบบ
                </div>
                <div class="card-body">
                        <div class="form-group">
                                <label for="usr">ชื่อผู้ใข้:</label>
                                <input type="text" class="form-control" id="usr" name="usr" required >
                        </div>
                        <div class="form-group">
                                <label for="pwd">รหัสผ่าน:</label>
                                <input type="password" class="form-control" id="pwd" name="pwd" required >
                        </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" type="submit" >เข้าระบบ</button>
                    <span id="show_error" class="text-danger"> </span>
    
                </div>
            </div> <!-- card -->
            </form>

            <div class="card mt-3">
                <div class="card-body p-0">
                    <div class="list-group">
                        <a href="?page=register" class="list-group-item list-group-item-action"> <i class="fa fa-user-plus text-info"></i> สมัครสมาชิก</a>
                    </div>
                </div>
            </div>
            
        </div>

    </div> <!-- row -->

<script>
$('#form_login').submit(function(event){
    event.preventDefault();
    
    var data = $( "#form_login" ).serialize();
    
    $.post("login_check.php",data,function(info){
        //alert(info);
        if(info=="ok") {
            window.location = '?page=main';
        } else {
            $('#show_error').html(info);
        }
        
    });
});
</script>