  
<!-- Image and text -->
<nav class="navbar navbar-expand-sm navbar-dark bg-danger sticky-top">
    <a class="navbar-brand" href="../../educationstd/" class="text-white">
      <img src="../../educationstd/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
       ระบบประกันคุณภาพการศึกษาในส่วนของการจัดการกิจกรรม
    </a>

    <!-- Links -->
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

  

</ul>

<form class="form-inline my-2 my-lg-0">
  <?php if($logined) { ?>
    <span class="text-light text-right">
    
    <a class="my-2 my-sm-0 text-light" href="../../educationstd/index.php?page=profile" data-toggle="tooltip" title="<?php echo $_SESSION['fullname']." (".$_SESSION['user_type_name'].")"; ?>" data-placement="bottom" > <i class="fas fa-address-book fa-2x"></i></a>
    &nbsp;&nbsp;&nbsp;
    <a class="my-2 my-sm-0 text-light" id="bt_logout" href="#" data-toggle="tooltip" title="ออกจากระบบ" > <i class="fas fa-power-off fa-2x"></i></a>
    </span>
  <?php } ?>
  </form>
</nav> 


<script>


 $('#bt_logout').click(function(){

    Swal.fire({
      title: 'ยืนยันออกจากระบบ',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        //alert("ss");
        window.location = '../../educationstd/logout.php';
      }
    });
});

</script>

