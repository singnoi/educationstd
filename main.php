<?php
include './includes/config.php';
?>

<link rel="stylesheet" type="text/css" media="screen" href="./node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 250px;
  }
  </style>


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

  <?php
    if($logined) {
        if($user_type_id == '1') {
  ?>
      <li class="breadcrumb-item active" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

<?php 
        }
    }
?>

    </ol>

</nav>


<div class="container-fluid">
<div class="row">
<div class="col-lg-4">
<div id="demo" class="carousel slide" data-ride="carousel">

<!-- Indicators -->
<ul class="carousel-indicators">
  <li data-target="#demo" data-slide-to="0" class="active"></li>
  <li data-target="#demo" data-slide-to="1"></li>
  <li data-target="#demo" data-slide-to="2"></li>
</ul>

<!-- The slideshow -->
<div class="carousel-inner">
  <div class="carousel-item active">
    <img src="./img/information.jpg" alt="Los Angeles">
  </div>
  <div class="carousel-item">
    <img src="./img/main.jpg" alt="Chicago">
  </div>
  <div class="carousel-item">
    <img src="./img/mani3.jpg" alt="New York">
  </div>
</div>

<!-- Left and right controls -->
<a class="carousel-control-prev" href="#demo" data-slide="prev">
  <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
  <span class="carousel-control-next-icon"></span>
</a>

</div>
</div>

    <div class="col-lg-8">
    <div class="card">
            <div class="card-header bg-warning">
                <h3 class="card-title" >ประชาสัมพันธ์กิจกรรม/โครงการ</h3>
            </div>
            <div class="card-body p-1">
                <table class="table table-striped" id="tb1">
                    <thead class="bg-secondary text-white">
                        <tr>
                            <th width="100px">วันที่</th>
                            <th width="100px">ลงทะเบียน</th>
                            <th>รายละเอียดกิจกรรม/โครงการ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <td>xx/xx/xxxx</td>
                    <td><a href="index.php?page=pro_add" class="btn btn-outline-danger btn-sm"> 
                    <span class="spinner-grow spinner-grow-sm"></span>
                    ลงทะเบียน
                    </a></td>
                    <td>โครงการ....</td>
                    </tbody>
                </table>
                
            </div>
           
        </div>
    </div>
   
        
</div>
</div>

<script src="./node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="./node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>


<script>

function del_user(id) {
    var ok = confirm("ต้องการลบข้อมูลใช่หรือไม่");
    if(ok){
        $.post("del_user.php",{id: id},function(info){
            if(info=="ok"){
                $( "#tb1" ).load( "?page=main #tb1" );
            } else {
                alert(info);
            }
        });
    }
}

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดงรายการ _MENU_ ",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหากิจกรรม/โครงการ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});
</script>

