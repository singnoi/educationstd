<?php 
    if(isset($_GET['id'])){
        include '../includes/db_connect.php';
        $s_id = $_GET['id'];
        $sql = "SELECT * from tbl_strategy where strategy_id = '$s_id'";
        $r = $mysqli->query($sql) or die ($sql);
        $obj = $r->fetch_object();
        $s_no = $obj->strategy_no;
        $s_name = $obj->strategy_name;
    } else {
        $s_no = "";
        $s_name = "";
        $s_id = "";
    }
    ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=strategy_main"><i class="fa fa-book"></i> ยุทธศาสตร์</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-edit"></i> แก้ไขยุทธศาสตร์</li>

    </ol>

</nav>
<div class="row">
    <div class="col-12">


            <form id="form_edit">
            <input type="hidden" value="<?php echo $s_id;?>" name="strategy_id" >
            <input type="hidden" value="edit" name="action" >
               <div class="card">
                   <div class="card-header bg-success text-white">
                       <i class="fa fa-edit mr-2"></i> แก้ไขยุทธศาสตร์
                   </div>
                   <div class="card-body">
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ยุทธศาสตร์ที่:</label>
                                   <div class="col-sm-10">
                                   <input type="number" class="form-control" id="strategy_no" name="strategy_no" value="<?php echo $s_no;?>" required>
                                   </div>
                           </div>
   
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชื่อยุทธศาสตร์:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="ยุทธศาตร์" value="<?php echo $s_name;?>" required>
                                   </div>
                           </div>
                   </div>
                   <div class="card-footer">
                   <button class="btn btn-primary" type="submit">บันทึก</button>
                   <span id="show_error_edit" class="text-danger"> </span>
                   </div>
               </div>
           </form>
    
    </div>
</div>

<script>
$('#form_edit').submit(function(e){
    e.preventDefault();
    $.post("strategy_action.php",$('#form_edit').serialize(),function(info){
        if(info=='ok'){
            alert("แก้ไขข้อมูลสำเร็จ");

            //$('#show_edit').hide();
            //$('#show_list').load("strategy_list.php");
            window.location = '?page=strategy_main';
        } else {
            $('#show_error_edit').html(info);
        }
    });
});

</script>