<form id="form_add">
               
            <div class="card">
                
                <div class="card-body">
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">แผนงานที่:</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" id="plan_no" name="plan_no" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อแผนงาน:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="plan_name" name="plan_name" placeholder="แผนงาน" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">อยู่ในยุทธศาสตร์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="strategy_id" name="strategy_id" required>
                                    <option value="" selected>  เลือกยุทธศาสตร์ </option>
                                    <?php 
                                    include '../includes/db_connect.php';
                                    $q = "SELECT * from tbl_strategy order by strategy_no ASC";
                                    $r = $mysqli->query($q) or die ($q);
                                    $n = $r->num_rows;
                                    if($n >0){
                                        while ($arr = $r->fetch_object()) {
                                            echo "<option value=\"{$arr->strategy_id}\" >{$arr->strategy_no}) {$arr->strategy_name} </option>";
                                        }
                                    }
                                    ?>
                                </select>
                                </div>
                        </div>

                        <div id="select_tactics">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">อยู่ในกลยุทธ์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control"  required>
                                </select>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span>
                </div>
            </div>
        </form>

<script>
$('#form_add').submit(function(e){
    e.preventDefault();
    $.post("plan_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok'){
            alert("บันทึกสำเร็จ");

            //$('#show_add').hide();
            //$('#show_list').load("plan_list.php");
            window.location = '?page=plan_main';
        } else {
            $('#show_error').html(info);
        }
    });
});

$('#strategy_id').change(function(){
    //alert("ssss");
    var s_id = $('#strategy_id').val();
    $('#select_tactics').load("select_tactics.php?strategy_id="+s_id);
});
</script>