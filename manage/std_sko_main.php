<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-book"></i> ตัวบ่งชี้ สกอ.</li>

    </ol>

</nav>
<div class="row">
    <div class="col-12" id="show_add">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success">
                    เพิ่มชื่อตัวบ่งชี้ สกอ.
                </div>
                <div class="card-body">
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ตัวบ่งชี้ที่:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="std_sko_no" name="std_sko_no" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อตัวบ่งชี้:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="std_sko_name" name="std_sko_name" placeholder="ชื่อตัวบ่งชี้" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชนิดตัวบ่งชี้:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="std_sko_type" name="std_sko_type" placeholder="ชนิดตัวบ่งชี้"  required>
                                   </div>
                           </div>
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ระดับตัวบ่งชี้:</label>
                                   <div class="col-sm-10">
                                   <select class="form-control" name="std_sko_level">
                                        <option value="1" selected > ระดับหลักสูตร </option>
                                        <option value="2"  > ระดับคณะ </option>
                                        <option value="3"  > ระดับมหาวิทยาลัย </option>
                                   </select>
                                   </div>
                           </div>


                </div>
                <div class="card-footer">
                <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span>
                </div>
            </div>
        </form>
    </div> <!-- col -->
    <div class="col-12" id="show_edit">
    
    </div> <!-- col -->
    <div class="col-12 mt-3" id="show_list">
       
    </div> <!-- col -->
</div> <!-- row -->
<script>
$('#show_add').hide();
$('#show_edit').hide();
$('#show_list').load("std_sko_list.php");
$('#form_add').submit(function(e){
    e.preventDefault();
    $.post("std_sko_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok'){
            alert("บันทึกสำเร็จ");
            $('#show_add').hide();
            $('#show_list').load("std_sko_list.php");
        } else {
            $('#show_error').html(info);
        }
    });
});
</script>