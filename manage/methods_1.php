<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success text-white">
                    เพิ่มกิจกรรม/โครงการ
                </div>
                <div class="card-body">
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">หลักการและเหตุผล:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="comment" placeholder="หลักการและเหตุผล"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">วัตถุประสงค์:</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="วัตถุประสงค์"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กลุ่มเป้าหมาย:</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="กลุ่มเป้าหมาย"></textarea>
                                </div>
                        </div>
                
                </div>
                <div class="card-footer">
                <a href="?page=methods" class="btn btn-primary"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                <a href="?page=methods_2" class="btn btn-primary">ถัดไป <i class="fa fa-arrow-right"></i></a>
                
                </div>
            </div>
        </form>
            
        </div>
    </div>
        
</div><!-- cont -->