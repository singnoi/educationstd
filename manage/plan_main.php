<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-book"></i> แผนงาน</li>

    </ol>

</nav>
<div class="row">
    <div class="col-12" id="show_add">
        
    </div> <!-- col -->
    <div class="col-12" id="show_edit">
    
    </div> <!-- col -->
    <div class="col-12 mt-3" id="show_list">
       
    </div> <!-- col -->
</div> <!-- row -->
<script>
$('#show_add').hide();
$('#show_edit').hide();
$('#show_list').load("plan_list.php");



</script>