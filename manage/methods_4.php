<div class="container-fluid">
        <div class="row">
                <div class="col-lg-12">
                
                <form id="form_add">
                <div class="card">
                        <div class="card-header bg-success text-white">
                        เพิ่มกิจกรรม/โครงการ
                        </div>
                        <div class="card-body">
                             <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ผลที่คาดว่าจะได้รับ:</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="ผลที่คาดว่าจะได้รับ"></textarea>
                                </div>
                             </div>
                             
                             <div class="form-group row">
                                <label class="col-sm-2 col-form-label">แผนบริหารความเสี่ยง (ของการจัดโครงการ):</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="แผนบริหารความเสี่ยง"></textarea>
                            </div>
                        </div>

                        </div>

                        <div class="col-lg-7">
                        <p>เอกสารแนบ</p> 
                        <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> เพิ่ม</button>
                        <br>
                                <div class="form-group">
                               <br> <input type="file" class="form-control-file border" name="file">
                                </div>

                        </div>
<br>
                        <div class="card-footer">
                        <a href="?page=methods_3" class="btn btn-primary"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                        <a href="?page=methods_5" class="btn btn-primary">ถัดไป <i class="fa fa-arrow-right"></i></a>
                <!-- <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span> -->
                        </div>
                </div>
                </form>


                </div>
        </div>   
</div><!-- cont -->