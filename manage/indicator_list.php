<?php
include '../includes/db_connect.php';
?>
<div class="card">
            <div class="card-header bg-warning">
              
                <h5 class="card-title" >รายการตัวชี้วัดทั้งหมด
                <a href="#" id="btn_add" class="float-right btn btn-primary btn-sm"> <i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</a>
                </h5>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="tb1">
                    <thead class="thead-dark">
                        <tr>
                            <th>ลำดับ</th><th>ชื่อตัวชี้วัด</th><th>แก้ไข</th><th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                     
                        $sql = "SELECT * from tbl_indicator order by indicator_id ASC";
                        $r = $mysqli->query($sql) or die ($sql);
                        $n = $r->num_rows;
                    
                        if($n > 0){
                            while ($arr = $r->fetch_object()) {
                                echo "<tr>";
                                echo "<td>{$arr->indicator_id}</td>";
                                echo "<td>{$arr->indicator_name}</td>";
                                ?>
                                <td>
                                <a href="#" onclick="s_edit('<?php echo $arr->indicator_id;?>'); return false;"> <i class="fa fa-edit text-warning"></i> </a>
                                </td>
                                <td>
                                <a href="#" onclick="s_del('<?php echo $arr->indicator_id;?>'); return false;"> <i class="fa fa-trash text-danger"></i> </a>
                                </td>
                                <?php 
                                echo "</tr>";
                            }
                        }

?>          
                    </tbody>
                </table>
            </div>
        </div> <!-- card -->

<script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script>
$('#btn_add').click(function(){
    //alert("ddd");
    $('#show_add').show();
});

function s_edit(id) {
    $('#show_edit').show();
    $('#show_edit').load("indicator_edit.php?id="+id);
}

function s_del(id) {
    var ok = confirm("ต้องการลบใช่หรือไม่");
    if(ok){
        $.post("indicator_action.php",{id: id, action: "del"},function(info){
            if(info=='ok'){
                $('#show_list').load("indicator_list.php");
            } else {
                alert("เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลได้");
            }
        });
    }
}
$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการตัวชี้วัด :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});
</script>