
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item " aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-tags"></i>  แบบประเมินโครงการ</li>

    </ol>

</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
            <div class="card-header bg-warning">
                    
                <h5 class="card-title"> แบบประเมินโครงการ
                    <a href="?page=methods" id="btn_add" class="float-right btn btn-primary btn-sm"> <i class="fa fa-plus"></i> เพิ่มกิจกรรม/โครงการ</a>
                </h5>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="tb1">
                    <thead class="thead-dark">
                        <tr>
                            <th>ลำดับ</th><th>วันที่</th><th>ชื่อกิจกรรม/โครงการ</th><th>แก้ไข</th><th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                        <a href="#" onclick="s_edit('<?php echo $arr->indicator_id;?>'); return false;"> <i class="fa fa-edit text-warning"></i> </a>
                        </td>
                        <td>
                        <a href="#" onclick="s_del('<?php echo $arr->indicator_id;?>'); return false;"> <i class="fa fa-trash text-danger"></i> </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            </div> <!-- card -->


        </div>
    </div>
</div>

<script>
$('#btn_add').click(function(){
    //alert("ddd");
    $('#show_add').show();
});

function s_edit(id) {
    $('#show_edit').show();
    $('#show_edit').load("methods_edit.php?id="+id);
}

function s_del(id) {
    var ok = confirm("ต้องการลบใช่หรือไม่");
    if(ok){
        $.post("medhods_action.php",{id: id, action: "del"},function(info){
            if(info=='ok'){
                $('#show_list').load("medhods_list.php");
            } else {
                alert("เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลได้");
            }
        });
    }
}
$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการตัวชี้วัด :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});
</script>

