<?php 
    if(isset($_GET['id'])){
        include '../includes/db_connect.php';
        $s_id = $_GET['id'];
        $sql = "SELECT * from tbl_standard_sko where std_sko_id = '$s_id'";
        $r = $mysqli->query($sql) or die ($sql);
        $obj = $r->fetch_object();
        $s_no = $obj->std_sko_no;
        $s_name = $obj->std_sko_name;
        $s_type = $obj->std_sko_type;
        $s_level = $obj->std_sko_level;
    } else {
        $s_no = "";
        $s_name = "";
        $s_id = "";
        $s_type = "";
        $s_level = "";
    }
    ?>
            <form id="form_edit">
            <input type="hidden" value="<?php echo $s_id;?>" name="std_sko_id" >
            <input type="hidden" value="edit" name="action" >
               <div class="card">
                   <div class="card-header bg-success">
                       แก้ไขตัวบ่งชี้ สกอ.
                   </div>
                   <div class="card-body">
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ตัวบ่งชี้ที่:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="std_sko_no" name="std_sko_no" value="<?php echo $s_no;?>" required>
                                   </div>
                           </div>
   
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชื่อตัวบ่งชี้:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="std_sko_name" name="std_sko_name" placeholder="ตัวบ่งชี้" value="<?php echo $s_name;?>" required>
                                   </div>
                           </div>
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชนิดตัวบ่งชี้:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="std_sko_type" name="std_sko_type" placeholder="ชนิดตัวบ่งชี้" value="<?php echo $s_type;?>" required>
                                   </div>
                           </div>
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ระดับตัวบ่งชี้:</label>
                                   <div class="col-sm-10">
                                   <select class="form-control" name="std_sko_level">
                                        <option value="1" <?php if($s_level=='1') echo "selected";?> > ระดับหลักสูตร </option>
                                        <option value="2" <?php if($s_level=='2') echo "selected";?> > ระดับคณะ </option>
                                        <option value="3" <?php if($s_level=='3') echo "selected";?> > ระดับมหาวิทยาลัย </option>
                                   </select>
                                   </div>
                           </div>
                   </div>
                   <div class="card-footer">
                   <button class="btn btn-primary" type="submit">บันทึก</button>
                   <span id="show_error_edit" class="text-danger"> </span>
                   </div>
               </div>
           </form>
<script>
$('#form_edit').submit(function(e){
    e.preventDefault();
    $.post("std_sko_action.php",$('#form_edit').serialize(),function(info){
        if(info=='ok'){
            alert("แก้ไขข้อมูลสำเร็จ");
            $('#show_edit').hide();
            $('#show_list').load("std_sko_list.php");
        } else {
            $('#show_error_edit').html(info);
        }
    });
});

</script>