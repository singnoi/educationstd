<div class="container-fluid">
        <div class="row">
                <div class="col-lg-12">
                
                <form id="form_add">
                <div class="card">
                        <div class="card-header bg-success text-white">
                        เพิ่มกิจกรรม/โครงการ
                        </div>
                        <div class="card-body">
                       
                             <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ระยะเวลาดำเนินงาน:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="methods_name" name="methods_name" placeholder="ระยะเวลาดำเนินงาน" required>
                            </div>
                        </div>
                             <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กระบวนการ:</label>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ลำดับที่:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="do_id" name="do_id" placeholder="ลำดับที่" required>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">รายการ กิจกรรม:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="do_name" name="do_name" placeholder="รายการ กิจกรรม" required>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">วันเริ่มกิจกรรม:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="do_st_date" name="do_st_date" placeholder="วันเริ่มกิจกรรม" required>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">วันสิ้นสุดกิจกรรม:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="do_en_date" name="do_en_date" placeholder="วินสิ้นสุดกิจกรรม" required>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">สถานที่ดำเนินงาน:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="สถานที่ดำเนินงาน" required>
                                </div>
                        </div>
              <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> เพิ่ม</button> 
              </div>
                <div class="card-footer">
                <a href="?page=methods_1" class="btn btn-primary"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                <a href="?page=methods_3" class="btn btn-primary">ถัดไป <i class="fa fa-arrow-right"></i></a>
                </div>
                </div>
                </form>


                </div>
        </div>   
</div><!-- cont -->

<br>
<br>
<div class="container">
  <p>รายการวิธีปฏิบัติการตาม โครงการของกิจกรรม</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ระยะเวลาดำเนินงาน</th>
        <th>กระบวนการ</th>
        <th>ลำดับที่</th>
        <th>รายการ กิจกรรม</th>
        <th>วันสิ้นสุดกิจกรรม</th>
        <th>สถานที่ดำเนินงาน</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1.ค่าวิทยากร</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>2.ค่าอาหาร</td>
        <td>Moe</td>
        <td>mary@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>3.ป้ายไวนิล</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>4.ค่าเอกสารประกอบการบรรยาย</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>5.ของที่ระลึกมอบให้วิทยาการ (ซื้อที่ฝ่ายพัสดุ)</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>รวม</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

      </tr>
    </tbody>
  </table>
</div>