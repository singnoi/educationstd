<?php 
    if(isset($_GET['id'])){
        include '../includes/db_connect.php';
        $s_id = $_GET['id'];
        $sql = "SELECT * from tbl_indicator where indicator_id = '$s_id'";
        $r = $mysqli->query($sql) or die ($sql);
        $obj = $r->fetch_object();
        
        $s_name = $obj->indicator_name;
    } else {
     
        $s_name = "";
        $s_id = "";
    }
    ?>
            <form id="form_edit">
            <input type="hidden" value="<?php echo $s_id;?>" name="indicator_id" >
            <input type="hidden" value="edit" name="action" >
               <div class="card">
                   <div class="card-header bg-success">
                       แก้ไขชื่อตัวชี้วัด
                   </div>
                   <div class="card-body">
                        
   
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชื่อตัวชี้วัด:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="indicator_name" name="indicator_name" placeholder="ยุทธศาตร์" value="<?php echo $s_name;?>" required>
                                   </div>
                           </div>
                   </div>
                   <div class="card-footer">
                   <button class="btn btn-primary" type="submit">บันทึก</button>
                   <span id="show_error_edit" class="text-danger"> </span>
                   </div>
               </div>
           </form>
<script>
$('#form_edit').submit(function(e){
    e.preventDefault();
    $.post("indicator_action.php",$('#form_edit').serialize(),function(info){
        if(info=='ok'){
            alert("แก้ไขข้อมูลสำเร็จ");
            $('#show_edit').hide();
            $('#show_list').load("indicator_list.php");
        } else {
            $('#show_error_edit').html(info);
        }
    });
});

</script>