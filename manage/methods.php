<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success text-white">
                    เพิ่มกิจกรรม/โครงการ
                </div>
                <div class="card-body">
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อกิจกรรม/โครงการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="methods_name" name="methods_name" placeholder="(ภาษาไทย)" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อกิจกรรม/โครงการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="methods_eng" name="methods_eng" placeholder="(ภาษาอังกฤษ)" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ประเภทโครงการ:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ยุทธศาสตร์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กลยุทธ์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ตัวชี้วัดกลยุทธ์:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                       
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">อาจารย์ผู้รับผิดชอบ:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" id="comment" placeholder="อาจารย์ผู้รับผิดชอบ"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">วิทยากร:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" id="comment" placeholder="วิทยากร"></textarea>
                                </div>
                        </div>
                        
                       
                        
                </div>
                
               <div class="card-footer">
               <a href="?page=methods_1" class="btn btn-primary">ถัดไป <i class="fa fa-arrow-right"></i></a>
                <!-- <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span> -->
                </div> 
            </div>
        </form>
            
        </div>
    </div>
        
</div><!-- cont -->