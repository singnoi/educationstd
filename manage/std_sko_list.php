<?php
include '../includes/db_connect.php';
?>
<div class="card">
            <div class="card-header bg-warning">
              
                <h5 class="card-title" >รายการตัวบ่งชี้ สกอ. ทั้งหมด
                <a href="#" id="btn_add" class="float-right btn btn-primary btn-sm"> <i class="fa fa-plus"></i> เพิ่มตัวบ่งชี้</a>
                </h5>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="tb1">
                    <thead class="thead-dark">
                        <tr>
                            <th>ลำดับ</th><th>ชื่อตัวบ่งชี้</th><th>ชนิดตัวบ่งชี้</th><th>ระดับ</th><th>แก้ไข</th><th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                     
                        $sql = "SELECT * from tbl_standard_sko order by std_sko_no ASC";
                        $r = $mysqli->query($sql) or die ($sql);
                        $n = $r->num_rows;
                    
                        if($n > 0){
                            while ($arr = $r->fetch_object()) {
                                if($arr->std_sko_level=='1') $s_level = "ระดับหลักสูตร";
                                if($arr->std_sko_level=='2') $s_level = "ระดับคณะ";
                                if($arr->std_sko_level=='3') $s_level = "ระดับมหาวิทยาลัย";
                                echo "<tr>";
                                echo "<td>{$arr->std_sko_no}</td>";
                                echo "<td>{$arr->std_sko_name}</td>";
                                echo "<td>{$arr->std_sko_type}</td>";
                                echo "<td>{$s_level}</td>";
                                ?>
                                <td>
                                <a href="#" onclick="s_edit('<?php echo $arr->std_sko_id;?>'); return false;"> <i class="fa fa-edit text-warning"></i> </a>
                                </td>
                                <td>
                                <a href="#" onclick="s_del('<?php echo $arr->std_sko_id;?>'); return false;"> <i class="fa fa-trash text-danger"></i> </a>
                                </td>
                                <?php 
                                echo "</tr>";
                            }
                        }

?>          
                    </tbody>
                </table>
            </div>
        </div> <!-- card -->

<script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script>
$('#btn_add').click(function(){
    //alert("ddd");
    $('#show_add').show();
});

function s_edit(id) {
    $('#show_edit').show();
    $('#show_edit').load("std_sko_edit.php?id="+id);
}

function s_del(id) {
    var ok = confirm("ต้องการลบใช่หรือไม่");
    if(ok){
        $.post("std_sko_action.php",{id: id, action: "del"},function(info){
            if(info=='ok'){
                $('#show_list').load("std_sko_list.php");
            } else {
                alert("เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลได้");
            }
        });
    }
}
$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการตัวบ่งชี้ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});
</script>