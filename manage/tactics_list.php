<?php
include '../includes/db_connect.php';
?>
<div class="card">
            <div class="card-header bg-warning">
              
                <h5 class="card-title" >รายการกลยุทธ์ทั้งหมด
                <a href="#" id="btn_add" class="float-right btn btn-primary btn-sm"> <i class="fa fa-plus"></i> เพิ่มกลยุทธ์</a>
                </h5>
            </div>
            <div class="card-body">

            <table class="table table-light">

            <?php
            $qs = " SELECT * from tbl_strategy order by strategy_no ASC";
            $rs = $mysqli->query($qs) or die ($qs);
            $ns = $rs->num_rows;

            if($ns > 0) {

// loop ยุทธศาสตร์ 
                while ($ars = $rs->fetch_object()) {
                

            ?>
           
                    <tr class="bg-success text-white">
                        <th> <h5> ยุทธศาสตร์ที่: <?php echo $ars->strategy_no.") ".$ars->strategy_name;?></h5></th>
                    </tr>
            
                    <tr>
                        <td>

            
                    <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                        <tr>
                            <th>ลำดับ</th><th>ชื่อกลยุทธ์</th><th class="text-center">แก้ไข</th><th class="text-center">ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                     
                        $sql = "SELECT * 
                        from tbl_tactics as t
                        join tbl_strategy as g
                        on g.strategy_id = t.strategy_id 
                        where t.strategy_id = '$ars->strategy_id' 
                        order by t.tactics_no ASC
                        ";
                        $r = $mysqli->query($sql) or die ($sql);
                        $n = $r->num_rows;
                    
                        if($n > 0){

                            $max_tac = 0;

            // Loop กลยุทธ์                
                            while ($arr = $r->fetch_object()) {
                                $max_tac = $arr->tactics_no + 1;
                                echo "<tr>";
                                echo "<td>".$arr->strategy_no.".".$arr->tactics_no."</td>";
                                echo "<td>{$arr->tactics_name}</td>";
                                ?>
                                <td class="text-center">
                                <a href="#" onclick="s_edit('<?php echo $arr->tactics_id;?>'); return false;" data-toggle="modal" data-target="#myModal" > <i class="fa fa-edit text-warning"></i> </a>
                                </td>
                                <td class="text-center">
                                <a href="#" onclick="s_del('<?php echo $arr->tactics_id;?>'); return false;"> <i class="fa fa-trash text-danger"></i> </a>
                                </td>
                                <?php 
                                echo "</tr>";
                            }
                        }
                        ?>   
                        <form method="post" id="form_<?php echo $ars->strategy_id;?>" >
                            <input type="hidden" id="strategy_id_" value="<?php echo $ars->strategy_id;?>" >
                       
                        <tr>
                            <td width="80">
                            
                            <input type="number" min="<?php echo $max_tac;?>" class="form-control" value="<?php echo $max_tac;?>" id="tactics_no_<?php echo $ars->strategy_id;?>" > 
                            </td>

                            <td>
                            <input type="text" class="form-control" value="" id="tactics_name_<?php echo $ars->strategy_id;?>" >
                            </td>

                            <td class="text-center">
                            <button type="button" onclick="add_tactics('<?php echo $ars->strategy_id;?>');" class="btn btn-primary btn-sm "> <i class="fa fa-plus"></i> </button>
                            </td>
                            <td></td>
                        </tr>

                        </form>

                    </tbody>
                </table>
        
            <?php 
            } // loop กล
            ?>
                        </td>
                    </tr>
        <?php 
        } // loop ยุท
        ?>
</table>

                
            </div>
        </div> <!-- card -->


<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-success text-white">
          <h4 class="modal-title">แก้ไขกลยุทธ์</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Modal body..
        </div>
        
       
        
      </div>
    </div>
  </div>        

<script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script>
$('#btn_add').click(function(){
    //alert("ddd");
    $('#show_add').show();
});

function s_edit(id) {
    //$('#show_edit').show();
    $('.modal-body').load("tactics_edit.php?id="+id);
}

function s_del(id) {
    var ok = confirm("ต้องการลบใช่หรือไม่");
    if(ok){
        $.post("tactics_action.php",{id: id, action: "del"},function(info){
            if(info=='ok'){
                $('#show_list').load("tactics_list.php");
            } else {
                alert("เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลได้");
            }
        });
    }
}
$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการยุทธศาสตร์ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});

function add_tactics(id) {
    var ok = confirm("บันทึก");
    if(ok){

        var tactics_no = $('#tactics_no_'+id).val();
        var tactics_name = $('#tactics_name_'+id).val();

        if(tactics_no == '') {
            alert("ต้องกรอกข้อมูลให้ครบถ้วนก่อนนะคะ");
            return false;
        }
        if(tactics_name == '') {
            alert("ต้องกรอกข้อมูลให้ครบถ้วนก่อนนะคะ");
            return false
        }

        $.post("tactics_action.php",{strategy_id: id, tactics_no: tactics_no, tactics_name: tactics_name},function(info){
            if(info == 'ok'){
                window.location = '?page=tactics_main';
            } else {
                alert(info);
            }
        });
    }
}
</script>