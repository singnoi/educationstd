<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-book"></i> กลยุทธ์</li>

    </ol>

</nav>
<div class="row">
    <div class="col-12" id="show_add">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success">
                    เพิ่มชื่อกลยุทธ์
                </div>
                <div class="card-body">
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กลยุทธ์ที่:</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" id="tactics_no" name="tactics_no" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อกลยุทธ์:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="tactics_name" name="tactics_name" placeholder="กลยุทธ์" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">อยู่ในยุทธศาสตร์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="strategy_id" required>
                                    <option value="" selected>  เลือกยุทธศาสตร์ </option>
                                    <?php 
                                    include '../includes/db_connect.php';
                                    $q = "SELECT * from tbl_strategy order by strategy_no ASC";
                                    $r = $mysqli->query($q) or die ($q);
                                    $n = $r->num_rows;
                                    if($n >0){
                                        while ($arr = $r->fetch_object()) {
                                            echo "<option value=\"{$arr->strategy_id}\" >{$arr->strategy_no}) {$arr->strategy_name} </option>";
                                        }
                                    }
                                    ?>
                                </select>
                                </div>
                        </div>
                </div>
                <div class="card-footer">
                <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span>
                </div>
            </div>
        </form>
    </div> <!-- col -->
    <div class="col-12" id="show_edit">
    
    </div> <!-- col -->
    <div class="col-12 mt-3" id="show_list">
       
    </div> <!-- col -->
</div> <!-- row -->
<script>
$('#show_add').hide();
$('#show_edit').hide();
$('#show_list').load("tactics_list.php");
$('#form_add').submit(function(e){
    e.preventDefault();
    $.post("tactics_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok'){
            alert("บันทึกสำเร็จ");
            $('#show_add').hide();
            $('#show_list').load("tactics_list.php");
        } else {
            $('#show_error').html(info);
        }
    });
});
</script>