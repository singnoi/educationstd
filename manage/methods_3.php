<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success text-white">
                    เพิ่มกิจกรรม/โครงการ
                </div>
                <div class="card-body">

                                <h6>งบประมาณในการดำเนินงาน</h6><br>
                       
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">รายการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="methods_name" name="methods_name" placeholder="รายการ" required>
                                </div>
                        </div>

                        <div class="row">
                                <div class="col-3">
                                        <div class="form-group">
                                                <label class="col-form-label">หน่วยละ:</label>
                                                
                                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="หน่วยละ" required>
                                               
                                        </div>
                                </div> <!-- col-->

                                <div class="col-3">
                                        <div class="form-group">
                                                <label class="col-form-label">จำนวน:</label>
                                                
                                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="จำนวน" required>
                                                
                                        </div>
                                </div> <!-- col-->

                                <div class="col-3">
                                        <div class="form-group">
                                                <label class="col-form-label">หน่วยนับ:</label>
                                                
                                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="หน่วยนับ" required>
                                                
                                        </div>
                                </div> <!-- col-->

                                <div class="col-3">
                                        <div class="form-group">
                                                <label class=" col-form-label">ค่าใช้จ่าย:</label>
                                                
                                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="ค่าใช้จ่าย" required>
                                                
                                        </div>
                                </div> <!-- col-->
                                
                        </div>

                <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> เพิ่ม</button>
                </div>
                <div class="card-footer">
                <a href="?page=methods_2" class="btn btn-primary"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                <a href="?page=methods_4" class="btn btn-primary">ถัดไป <i class="fa fa-arrow-right"></i></a>
                <!-- <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span> -->
                </div>
            </div>
        </form>
            
        </div>
    </div>
        
</div><!-- cont -->

<br>
<br>
<div class="container">
  <p>รายละเอียดค่าใช้จ่าย</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>รายการ</th>
        <th>หน่วยละ</th>
        <th>จำนวน</th>
        <th>หน่วยนับ</th>
        <th>ค่าใช้จ่าย</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1.ค่าวิทยากร</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>2.ค่าอาหาร</td>
        <td>Moe</td>
        <td>mary@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>3.ป้ายไวนิล</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>4.ค่าเอกสารประกอบการบรรยาย</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>5.ของที่ระลึกมอบให้วิทยาการ (ซื้อที่ฝ่ายพัสดุ)</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>รวม</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

      </tr>
    </tbody>
  </table>
</div>