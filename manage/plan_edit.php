<?php 
    if(isset($_GET['id'])){
        include '../includes/db_connect.php';
        $t_id = $_GET['id'];
        $sql = "SELECT * 
        from tbl_plan as t 
        join tbl_tactics as tt on tt.tactics_id = t.tactics_id 
        inner join tbl_strategy as s on s.strategy_id = t.strategy_id 
        where t.plan_id = '$t_id'";
        $r = $mysqli->query($sql) or die ($sql);
        $obj = $r->fetch_object();
        $t_no = $obj->plan_no;
        $t_name = $obj->plan_name;
        $s_id = $obj->strategy_id;
    } else {
        $t_no = "";
        $t_name = "";
        $t_id = "";
        $s_id = "";
    }
    ?>
            <form id="form_edit">
            <input type="hidden" value="<?php echo $t_id;?>" name="plan_id" >
            <input type="hidden" value="edit" name="action" >
               <div class="card">
                   
                   <div class="card-body">
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">แผนงานที่:</label>
                                   <div class="col-sm-10">
                                   <input type="number" class="form-control" id="plan_no" name="plan_no" value="<?php echo $t_no;?>" required>
                                   </div>
                           </div>
   
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชื่อแผนงาน:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="plan_name" name="plan_name" placeholder="แผนงาน" value="<?php echo $t_name;?>" required>
                                   </div>
                           </div>

                           <div class="form-group row">                               
                            <input type="hidden" name="tactics_id" value="<?php echo $obj->tactics_id;?>" >
                                <label class="col-sm-2 col-form-label">อยู่ในกลยุทธ์ที่:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="<?php echo $obj->tactics_name;?>" readonly >
                                    </div>
                            </div>


                           <div class="form-group row">                               
                           <input type="hidden" name="strategy_id" value="<?php echo $s_id;?>" >
                                <label class="col-sm-2 col-form-label">อยู่ในยุทธศาสตร์ที่:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php echo $obj->strategy_name;?>" readonly>
                                </select>
                                </div>
                        </div>

                   </div>
                   <div class="card-footer">
                   <button class="btn btn-primary" type="submit">บันทึก</button>
                   <span id="show_error_edit" class="text-danger"> </span>
                   </div>
               </div>
           </form>
<script>
$('#form_edit').submit(function(e){
    e.preventDefault();
    $.post("plan_action.php",$('#form_edit').serialize(),function(info){
        if(info=='ok'){
            alert("แก้ไขข้อมูลสำเร็จ");

            //$('#show_edit').hide();
            //$('#show_list').load("plan_list.php");
            window.location = '?page=plan_main';

        } else {
            $('#show_error_edit').html(info);
        }
    });
});

</script>