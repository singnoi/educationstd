<?php 
    if(isset($_GET['id'])){
        include '../includes/db_connect.php';
        $s_id = $_GET['id'];
        $sql = "SELECT * from tbl_unit where unit_id = '$s_id'";
        $r = $mysqli->query($sql) or die ($sql);
        $obj = $r->fetch_object();
        
        $s_name = $obj->unit_name;
    } else {
        
        $s_name = "";
        $s_id = "";
    }
    ?>
            <form id="form_edit">
            <input type="hidden" value="<?php echo $s_id;?>" name="unit_id" >
            <input type="hidden" value="edit" name="action" >
               <div class="card">
                   <div class="card-header bg-success">
                       แก้ไขหน่วยวัด
                   </div>
                   <div class="card-body">
                          
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชื่อหน่วยวัด:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="unit_name" name="unit_name" placeholder="หน่วยวัด" value="<?php echo $s_name;?>" required>
                                   </div>
                           </div>
                   </div>
                   <div class="card-footer">
                   <button class="btn btn-primary" type="submit">บันทึก</button>
                   <span id="show_error_edit" class="text-danger"> </span>
                   </div>
               </div>
           </form>
<script>
$('#form_edit').submit(function(e){
    e.preventDefault();
    $.post("unit_action.php",$('#form_edit').serialize(),function(info){
        if(info=='ok'){
            alert("แก้ไขข้อมูลสำเร็จ");
            $('#show_edit').hide();
            $('#show_list').load("unit_list.php");
        } else {
            $('#show_error_edit').html(info);
        }
    });
});

</script>