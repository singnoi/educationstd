<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success text-white">
                    แก้ไขกิจกรรม/โครงการ
                </div>
                <div class="card-body">
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อกิจกรรม/โครงการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="methods_name" name="methods_name" placeholder="(ภาษาไทย)" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ชื่อกิจกรรม/โครงการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="methods_eng" name="methods_eng" placeholder="(ภาษาอังกฤษ)" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ประเภทโครงการ:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ยุทธศาสตร์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กลยุทธ์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ตัวชี้วัดกลยุทธ์:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ตัวบ่งชี้ สกอ.:</label>
                                <div class="col-sm-10">
                                <select class="form-control" id="methods_type">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">อาจารย์ผู้รับผิดชอบ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="owner_user_id" name="owner_user_id" placeholder="ชื่ออาจารย์" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ผู้ช่วยอาจารย์:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="slave_user_id" name="slave_user_id" placeholder="ผู้ช่วยอาจารย์" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">วิทยากร:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="owner_etc" name="owner_etc" placeholder="วิทยากร" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">หลักการและเหตุผล:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="comment" placeholder="หลักการและเหตุผล"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">วัตถุประสงค์:</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="วัตถุประสงค์"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กลุ่มเป้าหมาย:</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="กลุ่มเป้าหมาย"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ระยะเวลาดำเนินการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="ระยะเวลาดำเนินการ" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">สถานที่ดำเนินงาน:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="สถานที่ดำเนินงาน" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">งบประมาณในการดำเนินงาน:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="งบประมาณในการดำเนินงาน" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ผลที่คาดว่าจะได้รับ:</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="comment" placeholder="ผลที่คาดว่าจะได้รับ"></textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">กำหนดการ:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="กำหนดการ" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-2 col-form-label">ปีการศึกษา:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="strategy_name" name="strategy_name" placeholder="ปีการศึกษา" required>
                                </div>
                        </div>

                </div>
                <div class="card-footer">
                <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span>
                </div>
            </div>
        </form>
            
        </div>
    </div>
        
</div><!-- cont -->