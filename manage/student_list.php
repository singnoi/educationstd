<?php
include '../includes/config.php';
?>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item " aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-user"></i> นักศึกษา </li>

    </ol>

</nav>


<div class="row">
    <div class="col-md-12">
       
        <div class="card">
            <div class="card-header bg-warning">
                <h5 class="card-title" >รายชื่อนักศึกษาทั้งหมด
                <a href="../?page=register&user_type_id=2" class="float-right btn btn-primary btn-sm"> <i class="fa fa-user-plus"></i> เพิ่มนักศึกษา</a>
                </h3>
            </div>
            <div class="card-body p-1">
                <table class="table table-striped" id="tb1">
                    <thead class="bg-secondary text-white">
                        <tr>
                            <th>รหัส</th><th>ชื่อสกุล</th><th>ชื่อเข้าใช้</th><th>รหัสผ่าน</th><th>เบอร์โทร</th><th>อีเมล์</th><th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $q = "SELECT * from tbl_user where user_type_id = '2' order by user_code DESC ";
                    $r = $con->query($q) or die ($q);
                    $n = $r->num_rows;
                    if($n >0) {
                        while ($obj = $r->fetch_object()) {
                            echo "<tr>";
                            echo "<td>".$obj->user_code."</td>";
                            echo "<td>".$obj->tname.$obj->fname." ".$obj->lname."</td>";
                            echo "<td>".$obj->username."</td>";
                            echo "<td>".$obj->user_password."</td>";
                            echo "<td>".$obj->user_tel."</td>";
                            echo "<td>".$obj->user_email."</td>";
                            ?>
                            <td>
                                <a href="../?page=profile&user_id=<?php echo $obj->user_id;?>"  class="btn btn-warning btn-sm" > <i class="fa fa-edit"></i> </a>
                                <a href="#" onclick="del_user('<?php echo $obj->user_id;?>'); return false;" class="btn btn-danger btn-sm" > <i class="fa fa-trash"></i> </a>
                            </td>
                            <?php 
                            echo "</tr>";
                        }
                    }
                    ?>
                    </tbody>
                </table>
                
            </div>
            
        </div>
        
    </div>
</div>
<script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script>

function del_user(id) {
    var ok = confirm("ต้องการลบข้อมูลใช่หรือไม่");
    if(ok){
        $.post("del_user.php",{id: id},function(info){
            if(info=="ok"){
                $( "#tb1" ).load( "?page=student_list #tb1" );
            } else {
                alert(info);
            }
        });
    }
}

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายชื่อนักศึกษา :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});
</script>
