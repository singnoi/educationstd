
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page"><a href="../../educationstd/index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

      <li class="breadcrumb-item active" aria-current="page"><a href="../../educationstd/manage/index.php?page=main"><i class="fa fa-lock"></i> เมนูผู้ดูแลระบบ</a></li>

    </ol>

</nav>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-6">
        <div class="card">
                <div class="card-header bg-success text-white">
                <i class="fa fa-folder-open"></i> จัดการข้อมูลผู้ใช้งาน
                </div>
                <div class="card-body p-0">
                    <div class="list-group">
                    <a href="index.php?page=teacher" class="list-group-item list-group-item-action"><i class="fa fa-users"></i> ข้อมูลอาจารย์</a>
                    <a href="index.php?page=student_list" class="list-group-item list-group-item-action"><i class="fa fa-users"></i> ข้อมูลนักศึกษา</a>
                    <a href="index.php?page=person_list" class="list-group-item list-group-item-action"><i class="fa fa-users"></i> บุคคลภายนอก</a>
                    </div>
                </div>
            </div>

            <br>

            <div class="card">
                <div class="card-header bg-success text-white">
                <i class="fa fa-folder-open"></i> จัดการข้อมูลพื้นฐาน
                </div>
                <div class="card-body p-0">
                    <div class="list-group">
                    <a href="index.php?page=strategy_main" class="list-group-item list-group-item-action"><i class="fa fa-book"></i> ยุทธศาสตร์</a>
                    <a href="index.php?page=tactics_main" class="list-group-item list-group-item-action"><i class="fa fa-book"></i> กลยุทธ์</a>
                    <a href="index.php?page=indicator_main" class="list-group-item list-group-item-action"><i class="fa fa-book"></i> ตัวชี้วัดสำหรับกลยุทธ์</a>
                    <a href="index.php?page=unit_main" class="list-group-item list-group-item-action"><i class="fa fa-book"></i> หน่วยวัด</a>
                    <a href="index.php?page=plan_main" class="list-group-item list-group-item-action"><i class="fa fa-book"></i> แผนงาน</a>
                    <a href="index.php?page=std_sko_main" class="list-group-item list-group-item-action"><i class="fa fa-book"></i> ตัวบ่งชี้ สกอ.</a>
                
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-success text-white">
                <i class="fa fa-folder-open"></i> จัดการกิจกรรม/โครงการ
                </div>
                <div class="card-body p-0">
                    <div class="list-group">
                    <a href="index.php?page=methods_list" class="list-group-item list-group-item-action"><i class="fa fa-tags"></i> รายการกิจกรรม/โครงการ</a>
                    <a href="index.php?page=assesment_list" class="list-group-item list-group-item-action"><i class="fa fa-tags"></i> สร้างแบบประเมิน</a>

                    </div>
                </div>

                
            </div>
        </div>
    </div>
        
</div><!-- cont -->