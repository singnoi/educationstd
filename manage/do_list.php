<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
            <div class="card-header bg-warning">
                    
                <h5 class="card-title" >ระยะเวลาดำเนินการ
                    <a href="?page=do" id="btn_add" class="float-right btn btn-primary btn-sm"> <i class="fa fa-plus"></i> เพิ่มระยะเวลาดำเนินการ</a>
                </h5>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="tb1">
                    <thead class="thead-dark">
                        <tr>
                            <th>ลำดับ</th><th>รายการ กิจกรรม</th><th>ระยะเวลา</th><th>แก้ไข</th><th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                        <a href="#" onclick="s_edit('<?php echo $arr->indicator_id;?>'); return false;"> <i class="fa fa-edit text-warning"></i> </a>
                        </td>
                        <td>
                        <a href="#" onclick="s_del('<?php echo $arr->indicator_id;?>'); return false;"> <i class="fa fa-trash text-danger"></i> </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            </div> <!-- card -->


        </div>
    </div>
</div>