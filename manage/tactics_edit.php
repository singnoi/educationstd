<?php 
    if(isset($_GET['id'])){
        include '../includes/db_connect.php';
        $t_id = $_GET['id'];
        $sql = "SELECT * from tbl_tactics as t inner join tbl_strategy as s on s.strategy_id = t.strategy_id where t.tactics_id = '$t_id'";
        $r = $mysqli->query($sql) or die ($sql);
        $obj = $r->fetch_object();
        $t_no = $obj->tactics_no;
        $t_name = $obj->tactics_name;
        $s_id = $obj->strategy_id;
    } else {
        $t_no = "";
        $t_name = "";
        $t_id = "";
        $s_id = "";
    }
    ?>
            <form id="form_edit">
            <input type="hidden" value="<?php echo $t_id;?>" name="tactics_id" >
            <input type="hidden" value="edit" name="action" >
               <div class="card">
                   
                   <div class="card-body">
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">กลยุทธ์ที่:</label>
                                   <div class="col-sm-10">
                                   <input type="number" class="form-control" id="tactics_no" name="tactics_no" value="<?php echo $t_no;?>" required>
                                   </div>
                           </div>
   
                           <div class="form-group row">
                                   <label class="col-sm-2 col-form-label">ชื่อกลยุทธ์:</label>
                                   <div class="col-sm-10">
                                   <input type="text" class="form-control" id="tactics_name" name="tactics_name" placeholder="ยุทธศาตร์" value="<?php echo $t_name;?>" required>
                                   </div>
                           </div>

                           <div class="form-group row">
                                <input type="hidden" name="strategy_id" value="<?php echo $s_id;?>" >
                                <label class="col-sm-2 col-form-label">อยู่ในยุทธศาสตร์ที่:</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="strategy_id1" required disabled>
                                    <option value="" >  เลือกยุทธศาสตร์ </option>
                                    <?php 
                                   
                                    $q = "SELECT * from tbl_strategy order by strategy_no ASC";
                                    $r = $mysqli->query($q) or die ($q);
                                    $n = $r->num_rows;
                                    if($n >0){
                                        while ($arr = $r->fetch_object()) {
                                            if($arr->strategy_id == $s_id) $sl = " selected ";
                                            else $sl = "";
                                            echo "<option value=\"{$arr->strategy_id}\" $sl >{$arr->strategy_no}) {$arr->strategy_name} </option>";
                                        }
                                    }
                                    ?>
                                </select>
                                </div>
                        </div>

                   </div>
                   <div class="card-footer">
                   <button class="btn btn-primary" type="submit">บันทึก</button>
                   <span id="show_error_edit" class="text-danger"> </span>
                   </div>
               </div>
           </form>
<script>
$('#form_edit').submit(function(e){
    e.preventDefault();
    $.post("tactics_action.php",$('#form_edit').serialize(),function(info){
        if(info=='ok'){
            alert("แก้ไขข้อมูลสำเร็จ");

            //$('#show_edit').hide();
            //$('#show_list').load("tactics_list.php");
            window.location = '?page=tactics_main'
        } else {
            $('#show_error_edit').html(info);
        }
    });
});

</script>