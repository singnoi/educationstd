<div class="container-fluid mt-3">
  <h5><i class="fa fa-magic"></i> สร้างกิจกรรม/โครงการ</h5>
  <br>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" href="#home">สร้างกิจกรรม/โครงการ</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu1">ขั้นตอนที่ 1</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu2">ขั้นตอนที่ 2</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu3">ขั้นตอนที่ 3</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu4">ขั้นตอนที่ 4</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu5">ขั้นตอนที่ 5</a>
    </li>
  
    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
      <?php
      include "methods.php";
      ?>
    </div>
    <div id="menu1" class="container tab-pane fade"><br>
    <?php
      include "methods_1.php";
      ?>
    </div>
    <div id="menu2" class="container tab-pane fade"><br>
    <?php
      include "methods_2.php";
      ?>
    </div>
    <div id="menu3" class="container tab-pane fade"><br>
    <?php
      include "methods_3.php";
      ?>
    </div>
    <div id="menu4" class="container tab-pane fade"><br>
    <?php
      include "methods_4.php";
      ?>
    </div>
    <div id="menu5" class="container tab-pane fade"><br>
    <?php
      include "methods_5.php";
      ?>
    </div>
    
    
  </div>
</div>

<script>
$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
</script>


