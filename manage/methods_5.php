<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
        <form id="form_add">
               
            <div class="card">
                <div class="card-header bg-success text-white">
                    เพิ่มกิจกรรม/โครงการ
                </div>
                <div class="card-body">

                  <h6>กำหนดการ การจัดกิจกรรม/โครงการ</h6><br>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">วันที่:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="methods_name" name="methods_name" placeholder="วันที่ เดือน พ.ศ." required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">เวลา:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="methods_name" name="methods_name" placeholder="xx.xx - xx.xx" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">รายละเอียด:</label>
                      <div class="col-sm-10">
                    <textarea class="form-control" rows="5" id="comment" placeholder="รายละเอียด"></textarea>
                  </div>  
                </div>
                <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> เพิ่ม</button>
               
            </div>
            <div class="card-footer">
                <a href="?page=methods_4" class="btn btn-primary"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                <button class="btn btn-primary" type="submit">บันทึก</button>
                <span id="show_error" class="text-danger"> </span>
                </div>
        </form>
            
        </div>
    </div>
        
</div><!-- cont -->

<br>
<br>
<div class="container">

  <p>รายละเอียดค่าใช้จ่าย</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
      <h6>วันที่ </h6>
        <th width="200px">เวลา</th>
        <th>รายละเอียด</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>9.00 - 9.30</td>
        <td>เปิดงานการอบรมเรื่อง.....</td>
      </tr>
      <tr>
        <td>9.30 - 9.40</td>
        <td>แนะนำวิทยากร ...</td>
      </tr>
      <tr>
        <td>9.40 - 11.00</td>
        <td>เริ่มการบรรยาย</td>
      </tr>
      
    </tbody>
  </table>
</div>