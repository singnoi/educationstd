<?php
include '../includes/db_connect.php';
?>
<div class="card">
            <div class="card-header bg-warning">
              
                <h5 class="card-title" >รายการแผนงานทั้งหมด
                <a href="#" id="btn_add" data-toggle="modal" data-target="#myModal1" class="float-right btn btn-primary btn-sm"> <i class="fa fa-plus"></i> เพิ่มแผนงาน</a>
                </h5>
            </div>
            <div class="card-body">
            
                    <table class="table table-striped table-sm" id="tb1">
                    <thead class="thead-light">
                        <tr>
                        <th>ยุทธศาสตร์</th><th>กลยุทธ์</th><th>แผนงานที่</th><th>ชื่อแผนงาน</th><th>แก้ไข</th><th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                     
                        $sql = "SELECT
                        *
                        FROM
                        tbl_tactics AS t
                        JOIN tbl_plan AS p
                        ON t.tactics_id = p.tactics_id 
                        JOIN tbl_strategy AS s
                        ON s.strategy_id = t.strategy_id
                        ORDER BY
                        p.strategy_id ASC,
                        p.tactics_id ASC,
                        p.plan_no ASC";
                        $r = $mysqli->query($sql) or die ($sql);
                        $n = $r->num_rows;
                    
                        if($n > 0){
                            while ($arr = $r->fetch_object()) {
                                echo "<tr>";
                                echo "<td>";
                                ?>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="<?php echo $arr->strategy_name;?>" >
                                <?php 
                                echo $arr->strategy_no;
                                ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>";
                                ?>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="<?php echo $arr->tactics_name;?>" >
                                <?php 
                                echo $arr->strategy_no.".".$arr->tactics_no;
                                ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>".$arr->plan_no."</td>";
                                echo "<td>{$arr->plan_name}</td>";
                                
                                
                                ?>
                                <td>
                                <a href="#" onclick="s_edit('<?php echo $arr->plan_id;?>'); return false;" data-toggle="modal" data-target="#myModal" > <i class="fa fa-edit text-warning"></i> </a>
                                </td>
                                <td>
                                <a href="#" onclick="s_del('<?php echo $arr->plan_id;?>'); return false;"> <i class="fa fa-trash text-danger"></i> </a>
                                </td>
                                <?php 
                                echo "</tr>";
                            }
                        }

?>          
                    </tbody>
                </table>
                   

                
            </div>
        </div> <!-- card -->


<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-success text-white">
          <h4 class="modal-title">แก้ไขแผนงาน</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Modal body..
        </div>
        
       
        
      </div>
    </div>
  </div>     

  <!-- The Modal -->
<div class="modal fade" id="myModal1">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-success text-white">
          <h4 class="modal-title">เพิ่มแผนงาน</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="md_body1">
          Modal body..
        </div>
        
       
        
      </div>
    </div>
  </div>      

<script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script>
$('#btn_add').click(function(){
    //alert("ddd");
    //$('#show_add').show();
    $('#md_body1').load("plan_form.php");
});

function s_edit(id) {
    //$('#show_edit').show();
    $('.modal-body').load("plan_edit.php?id="+id);
}

function s_del(id) {
    var ok = confirm("ต้องการลบใช่หรือไม่");
    if(ok){
        $.post("plan_action.php",{id: id, action: "del"},function(info){
            if(info=='ok'){
                $('#show_list').load("plan_list.php");
            } else {
                alert("เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลได้");
            }
        });
    }
}
$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการแผนงาน :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: true

});
</script>